﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Library.Enumerations
{
	public enum Enum1
	{
		[Display(Name = "Test value 1")]
		TestValue1 = 1,

		[Display(Name = "Test value 2")]
		TestValue2 = 2,

		[Display(Name = "Test value 3")]
		TestValue3 = 3,

		[Display(Name = "Test value 4")]
		TestValue4 = 4,
	}
}
