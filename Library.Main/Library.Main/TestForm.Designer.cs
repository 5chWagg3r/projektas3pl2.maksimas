﻿namespace Library.Main
{
	partial class TestForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtInt1 = new System.Windows.Forms.TextBox();
			this.cmbxEnum1 = new System.Windows.Forms.ComboBox();
			this.listboxValue1 = new System.Windows.Forms.ListBox();
			this.cmbxList2 = new System.Windows.Forms.ComboBox();
			this.listboxList1 = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// txtInt1
			// 
			this.txtInt1.Location = new System.Drawing.Point(50, 13);
			this.txtInt1.Name = "txtInt1";
			this.txtInt1.Size = new System.Drawing.Size(100, 20);
			this.txtInt1.TabIndex = 0;
			// 
			// cmbxEnum1
			// 
			this.cmbxEnum1.FormattingEnabled = true;
			this.cmbxEnum1.Location = new System.Drawing.Point(50, 53);
			this.cmbxEnum1.Name = "cmbxEnum1";
			this.cmbxEnum1.Size = new System.Drawing.Size(121, 21);
			this.cmbxEnum1.TabIndex = 1;
			// 
			// listboxValue1
			// 
			this.listboxValue1.FormattingEnabled = true;
			this.listboxValue1.Location = new System.Drawing.Point(50, 81);
			this.listboxValue1.Name = "listboxValue1";
			this.listboxValue1.Size = new System.Drawing.Size(120, 95);
			this.listboxValue1.TabIndex = 2;
			// 
			// cmbxList2
			// 
			this.cmbxList2.FormattingEnabled = true;
			this.cmbxList2.Location = new System.Drawing.Point(49, 182);
			this.cmbxList2.Name = "cmbxList2";
			this.cmbxList2.Size = new System.Drawing.Size(121, 21);
			this.cmbxList2.TabIndex = 3;
			this.cmbxList2.SelectedIndexChanged += new System.EventHandler(this.cmbxList2_SelectedIndexChanged);
			// 
			// listboxList1
			// 
			this.listboxList1.FormattingEnabled = true;
			this.listboxList1.Location = new System.Drawing.Point(177, 81);
			this.listboxList1.Name = "listboxList1";
			this.listboxList1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.listboxList1.Size = new System.Drawing.Size(120, 95);
			this.listboxList1.TabIndex = 4;
			// 
			// TestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(346, 261);
			this.Controls.Add(this.listboxList1);
			this.Controls.Add(this.cmbxList2);
			this.Controls.Add(this.listboxValue1);
			this.Controls.Add(this.cmbxEnum1);
			this.Controls.Add(this.txtInt1);
			this.Name = "TestForm";
			this.Text = "TestForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtInt1;
		private System.Windows.Forms.ComboBox cmbxEnum1;
		private System.Windows.Forms.ListBox listboxValue1;
		private System.Windows.Forms.ComboBox cmbxList2;
		private System.Windows.Forms.ListBox listboxList1;
	}
}