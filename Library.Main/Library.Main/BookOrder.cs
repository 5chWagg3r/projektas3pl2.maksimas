﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class BookOrder : Form
    {
        private BookOrderService _bookOrderService;
        private BookOrderViewModel _bookOrderViewModel;
        public BookOrder()
        {
            _bookOrderService = new BookOrderService();
            _bookOrderViewModel = new BookOrderViewModel();
            InitializeComponent();
            ConfigureDataGridView();
        }

        private void btnSaveBookReader_Click(object sender, EventArgs e)
        {
            BookReader bookReader = new BookReader(_bookOrderViewModel.BookReader);
            bookReader.FormClosed += BookReader_FormClosed;
            bookReader.ShowDialog();
        }

        private void BookReader_FormClosed(object sender, FormClosedEventArgs e)
        {
            var form = (BookReader)sender;
            lblFirstName.Text = !string.IsNullOrWhiteSpace(form.Firstname) ? form.Firstname : "-";
            lblLastName.Text = !string.IsNullOrWhiteSpace(form.Lastname) ? form.Lastname : "-";
            lblEmail.Text = !string.IsNullOrWhiteSpace(form.Email) ? form.Email : "-";
            lblPhone.Text = !string.IsNullOrWhiteSpace(form.Phone) ? form.Phone : "-";
            txtBookReaderId.Text = form.Id.ToString();

            _bookOrderViewModel.BookReader = new BookReaderViewModel
            {
                Id = form.Id,
                Email = form.Email,
                FirstName = form.Firstname,
                LastName = form.Lastname,
                Phone = form.Phone
            };
        }

        private void ConfigureDataGridView()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "Id",
                Visible = false,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridView1.Columns.Add(idColumn);

            DataGridViewTextBoxColumn bookNameColumn = new DataGridViewTextBoxColumn
            {
                Name = "BookName",
                HeaderText = "Book Name",
                Width = 150,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridView1.Columns.Add(bookNameColumn);

            DataGridViewTextBoxColumn TakeDateColumn = new DataGridViewTextBoxColumn
            {
                Name = "TakeDate",
                HeaderText = "Take Date",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100
            };
            dataGridView1.Columns.Add(TakeDateColumn);

            DataGridViewTextBoxColumn ReturnDateColumn = new DataGridViewTextBoxColumn
            {
                Name = "ReturnDate",
                HeaderText = "Return Date",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100
            };
            dataGridView1.Columns.Add(ReturnDateColumn);

            DataGridViewButtonColumn editReturnDateColumn = new DataGridViewButtonColumn
            {
                CellTemplate = new DataGridViewButtonCell(),
                Name = "EditReturnDate",
                Width = 70,
                HeaderText = "Return Date",
                UseColumnTextForButtonValue = true,
                Resizable = DataGridViewTriState.False
            };
            dataGridView1.Columns.Add(editReturnDateColumn);

        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            BookList bookList = new BookList(isShowAddBookButton: true);
            bookList.FormClosed += BookList_FormClosed;
            bookList.ShowDialog();
        }

        private void BookList_FormClosed(object sender, FormClosedEventArgs e)
        {
            var form = (BookList)sender;
            var orderBooks = form.OrderBooks;

            foreach (var orderBook in orderBooks)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["BookName"].Value = orderBook.Name;
                dataGridView1.Rows[index].Cells["TakeDate"].Value = orderBook.TakeDate;
                dataGridView1.Rows[index].Cells["ReturnDate"].Value = orderBook.ReturnDate;
                dataGridView1.Rows[index].Cells["EditReturnDate"].Value = "Edit Return Date";
            }
        }
    }
}
