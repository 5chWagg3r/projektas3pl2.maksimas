﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.CustomExeptions;
using Library.Services;
using Library.ViewModels;

namespace Library.Main
{
    public partial class BookReader : BaseForm
    {
        public int Id
        {
            get
            {
                return Convert.ToInt32(txtId.Text);
            }
        }

        public string Firstname
        {
            get
            {
                return txtFirstName.Text;
            }
        }

        public string Lastname
        {
            get
            {
                return txtLastName.Text;
            }
        }

        public string Phone
        {
            get
            {
                return txtPhone.Text;
            }
        }

        public string Email
        {
            get
            {
                return txtEmail.Text;
            }
        }

        private BookOrderService _bookOrderService;
        private BookReaderViewModel _bookReaderViewModel;
        public BookReader(BookReaderViewModel viewModel = null)
        {
            _bookOrderService = new BookOrderService();
            InitializeComponent();
            if (viewModel == null)
            {
                _bookReaderViewModel = new BookReaderViewModel();
            } else
            {
                _bookReaderViewModel = viewModel;
                FillForm(viewModel);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CheckPhoneNumber(string phoneNumber)
        {
            if (phoneNumber.Length != 12)
            {
                throw new InvalidPhoneNumberException();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CheckPhoneNumber(txtPhone.Text);
            }
            catch (InvalidPhoneNumberException ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return;
            }

            try
            {
                _bookReaderViewModel.FirstName = txtFirstName.Text;
                _bookReaderViewModel.LastName = txtLastName.Text;
                _bookReaderViewModel.Phone = txtPhone.Text;
                _bookReaderViewModel.Email = txtEmail.Text;
                _bookOrderService.SaveBookReader(_bookReaderViewModel);
                txtId.Text = _bookReaderViewModel.Id.ToString();
            }
            catch (FailedSaveBookReaderExeption ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

		private bool IsFormValid()
		{
			if (true) {

			}
			return true;
		}
    }
}
