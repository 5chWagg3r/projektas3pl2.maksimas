﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.Dal.DataAnnotations;

namespace Library.Main
{
    public partial class BaseForm : Form 
    {
        public BaseForm()
        {

        }

        public void FillForm<T>(T viewModel) where T : class
        {
            var properties = viewModel.GetType().GetProperties();

            foreach (var property in properties)
            {
                var customAttributes = property.GetCustomAttributes(false);

                if (customAttributes.Count() != 0)
                {
					var controlName = ((FormPropertyAttribute)customAttributes.Where(x => x.GetType() == typeof(FormPropertyAttribute)).FirstOrDefault()).ControlName;

					switch(GetPropertyType(property)) {
						case InputType.TextBox: {
								FillTextBox(property, controlName, viewModel);
							}
							break;
						case InputType.ComboBox: {
								SelectComboBoxValue(property, controlName, viewModel);
							}
							break;
						case InputType.ListBox: {
								SelectListBoxValues(property, controlName, viewModel);
							}
							break;
						default:
							throw new Exception("test");
					}
                }
            }
        }

		private InputType GetPropertyType(PropertyInfo property)
		{
			return ((FormPropertyAttribute)property.GetCustomAttributes(false).Where(x => x.GetType() == typeof(FormPropertyAttribute)).FirstOrDefault()).ControlType;
		}

		private void FillTextBox(PropertyInfo property, string controlName, object viewModel)
		{
			Controls[controlName].Text = property.GetValue(viewModel).ToString();
		}

		private void SelectComboBoxValue(PropertyInfo property, string controlName, object viewModel)
		{

			var comboBox = (ComboBox)Controls[controlName];
			if (!string.IsNullOrEmpty(comboBox.ValueMember)) {
				((ComboBox)Controls[controlName]).SelectedValue = property.GetValue(viewModel);
			} else {
				((ComboBox)Controls[controlName]).SelectedItem = property.GetValue(viewModel);
			}
			
		}

		private void SelectListBoxValues(PropertyInfo property, string controlName, object viewModel)
		{
			var listBox = (ListBox)Controls[controlName];
			var objectValue = property.GetValue(viewModel, null);

			if (!string.IsNullOrEmpty(listBox.ValueMember)) {
				if (listBox.SelectionMode == SelectionMode.One) {
					listBox.SelectedValue = objectValue;
				}
				else {
					var values = objectValue as IEnumerable;
					if (values != null) {
						foreach (var value in values) {
							var countOfElements = ((ListBox)Controls[controlName]).Items.Count;
							for (int i = 0; i < countOfElements; i++) {
							}
						}
					}
				}
			}
		}
    }
}
